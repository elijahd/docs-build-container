FROM python:3.6-alpine

RUN apk --no-cache --update add py3-pip python3-dev gcc freetype-dev libpng-dev build-base
RUN pip3 install ipython sphinx matplotlib
